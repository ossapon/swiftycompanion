//
//  ProjectCell.swift
//  SwiftyCompanion
//
//  Created by Alex Sapon on 2/16/19.
//  Copyright © 2019 Alex Sapon. All rights reserved.
//

import UIKit

class ProjectCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var markLabel: UILabel?
    
    
}
